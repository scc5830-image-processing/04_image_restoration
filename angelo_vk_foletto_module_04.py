# coding: utf-8

"""
    * Nome: Angelo Victor Kraemer Foletto
    * Numero USP: 12620258
    * Código do Curso: SCC5830 - Image Processing (2021) / 1
    * Docente: Moacir Antonelli Ponti
    * Assignment 4: Image Restoration
    * Data 09/06/2021
    * Create: 05/06/2021
    * Update: 07/06/2021
    *
    * https://gitlab.com/scc5830-image-processing/04_image_restoration
"""

## Imports
import numpy as np
import imageio as imgio
import math
from scipy.fft import rfft2, irfft2


class Module04:
    ## Initialize Static Variable
    __average = 'average'
    __robust = 'robust'

    __img_case5 = 'case5'
    __img_case6 = 'case6'
    __img_case7 = 'case7'
    __img_case8 = 'case8'

    def __init__(self, file_name_1, file_name_2, type_filter, gamma, mask, filter_size, denoising='', sigma=0.0):
        """
        Initialize dynamic variable

        :param file_name_1: String
        :param file_name_2: String
        :param type_filter: int
        :param gamma: float
        :param mask: String
        :param filter_size: int
        :param denoising: float
        :param sigma: float
        """
        self.img_reference = imgio.imread(file_name_1)
        self.img_degraded = imgio.imread(file_name_2)
        self.img_reference_name = file_name_1
        self.img_degraded_name = file_name_2
        self.type_filter = type_filter
        self.gamma = gamma
        self.mask = mask
        self.filter_size = filter_size
        self.denoising = denoising
        self.sigma = sigma

    def splitLine(self, line, text_split):
        """

        :param line: String
        :param text_split: String
        :return: float np.array
        """
        return np.array(list(map(np.int8, line.split(text_split)))).astype(np.int8)

    def normalize_min_max(self, value):
        """
        Apply method min-max in values parameter

        :param value: np.array
        :return: np.array
        """
        imin = np.min(value)
        imax = np.max(value)
        return (value - imin) * ((imax - imin) / (imax - imin)) + imin

    def sizePad(self, value):
        """

        :param value: Int
        :return: int
        """
        return int(value / 2)

    def filter1(self):
        """

        :return: np.array (new_image)
        """
        disp_n_or_irq = float()
        i1, i2, j1, j2 = self.splitLine(self.mask, ' ')
        crop_img = self.img_degraded[i1:i2, j1:j2]
        size_pad = self.sizePad(self.filter_size)
        swap_img = np.pad(self.img_degraded
                          , [(size_pad, size_pad), (size_pad, size_pad)]
                          , 'symmetric'
                          )
        size_swap_img = swap_img.shape[0] + 1
        result = np.zeros((self.img_degraded.shape[0], self.img_degraded.shape[0]), dtype=np.float64)

        if self.denoising.lower() in self.__average:  # Standard deviation
            disp_n_or_irq = self.dispN(crop_img)
        elif self.denoising.lower() in self.__robust:  # Quartil
            disp_n_or_irq = self.iqr(crop_img)  # 25% and 75%

        for i in range(self.filter_size, size_swap_img):
            swap_i = i - self.filter_size
            for j in range(self.filter_size, size_swap_img):
                swap_j = j - self.filter_size
                g = self.img_degraded[swap_i][swap_j]
                disp_l = self.dispL(swap_img, swap_i, i, swap_j, j, disp_n_or_irq)
                centr_l = self.centrL(swap_img, swap_i, i, swap_j, j)
                result[swap_i][swap_j] = g - self.gamma * (disp_n_or_irq / disp_l) * (g - centr_l)
        return result.astype(np.float64)

    def dispN(self, img):
        """
        If the estimated dispersion measure computed for an image is dispη = 0, manually set it to dispη = 1 to avoid
        having the filter doing nothing for the whole image.

        :param value: float
        :return: 1 or value
        """
        value = np.std(img)
        if value == 0:
            return 1
        return value

    def iqr(self, value, quartil=None):
        """
        Calc of quartil

        :param value: np.array
        :param quartil: None, list or int
        :return: float or int
        """
        if quartil is None:
            quartil = [25, 75]
        quartil_25, quartil_75 = np.percentile(value, quartil)  # 25% and 75%
        return quartil_75 - quartil_25

    def centrL(self, img, i1, i2, j1, j2):
        """
        Calc centrL

        :param img: np.array
        :param i1: int
        :param i2: int
        :param j1: int
        :param j2: int
        :return: float
        """
        if self.denoising.lower() in self.__average:
            return np.mean(img[i1:i2, j1:j2])
        elif self.denoising.lower() in self.__robust:
            return np.median(img[i1:i2, j1:j2])

    def dispL(self, img, i1, i2, j1, j2, disp_n_or_irq):
        """
        Calc dispL

        :param img: np.array
        :param i1: int
        :param i2: int
        :param j1: int
        :param j2: int
        :param disp_n_or_irq: float
        :return: float
        """
        swap = float()
        if self.denoising.lower() in self.__average:
            swap = np.std(img[i1:i2, j1:j2])
        elif self.denoising.lower() in self.__robust:
            swap = self.iqr(img[i1:i2, j1:j2])

        if swap == 0:
            return disp_n_or_irq
        return swap

    def filter2(self):
        """
        H é o filtro de degradação
        G é a imagem degradada
        P é o inverso do filtro Laplaciano
        H_Linha é o complexo de conjulgado
        """

        h = self.galssianFilter(self.filter_size, self.sigma)
        h = self.abs_rfft2(h, self.shape(h))

        """
        Por que h_line não funciona com a inversa?
        """
        # h_line = self.galssianFilterInverter(self.filter_size, self.sigma)
        h_line = self.galssianFilter(self.filter_size, self.sigma)
        h_line = self.abs_rfft2(h_line, self.shape(h_line))

        p = (self.laplacian())
        p = self.abs_rfft2(p, self.shape(p))

        g = self.abs_rfft2(self.img_degraded)

        result = (h_line / (np.power(h, 2) + self.gamma * np.power(p, 2))) * g
        return self.normalize_min_max(irfft2(result))

    def shape(self, value):
        return int(self.img_degraded.shape[0] // 2 - value.shape[0] // 2)

    def abs_rfft2(self, value, shape=None):
        if shape is None:
            return rfft2(value)
        return np.absolute(rfft2(np.pad(value
                                        , (shape, shape - 1)
                                        , 'constant'
                                        , constant_values=0
                                        )
                                 )
                           )

    def laplacian(self):
        """

        :return: np.array
        """
        return np.array([[0, -1, 0], [-1, 4, -1], [0, -1, 0]])

    def galssianFilter(self, filter_size, sigma):
        """

        :param filter_size: Int
        :param sigma: float
        :return: list
        """
        x, y = self.galssian(filter_size)
        filt = np.exp(-(1 / 2) * (np.square(x) + np.square(y)) / np.square(sigma))
        return filt / np.sum(filt)

    def galssianFilterInverter(self, filter_size, sigma):
        """

        :param h: np.array
        :return: np.array
        """
        x, y = self.galssian(filter_size)
        filt = np.exp((1 / 2) * (np.square(x) - np.square(y)) / np.square(sigma))
        return filt / np.sum(filt)

    def galssian(self, filter_size):
        arx = np.arange((-filter_size // 2) + 1.0, (filter_size // 2) + 1.0)
        return np.meshgrid(arx, arx)

    def calcRMSE(self, original, new_image):
        """
        Calc RMSE

        :return: float
        """
        pw = 2
        return math.sqrt(
            np.sum(np.power(np.subtract(original, new_image), pw))
            / (new_image.shape[0] * new_image.shape[1])
        )

    def filterMethod(self):
        """
        Select filter method input

        :return: new_image numpy array is type unit8
        """
        if self.type_filter == 1:
            return self.filter1()
        elif self.type_filter == 2:
            return self.filter2()

    def main(self):
        """
        Main method

        :return: Return RMSE calc
        """
        return self.calcRMSE(self.img_reference, self.filterMethod())


class Debug:
    mask = str()
    filter_size = int()
    denoising = str()
    sigma = float()

    def img1(self):
        ## img1
        file_name_1 = 'polygons128.png'
        file_name_2 = 'input_image/case1_10.png'
        type_filter = 1
        gamma = 0.15

        if type_filter == 1:  # method 1
            self.mask = '0 20 0 20'
            self.filter_size = 5
            self.denoising = 'average'
        module = Module04(file_name_1, file_name_2, type_filter, gamma, self.mask, self.filter_size)
        rmse = module.main()
        print('Img 1 (5.392600407980945): %.4f' % round(rmse, 4))

    def img2(self):
        ## img2
        file_name_1 = 'input_image/polygons128.png'
        file_name_2 = 'input_image/case2_45.png'
        type_filter = 1
        gamma = 0.95

        if type_filter == 1:  # method 1
            mask = '0 20 0 20'
            filter_size = 5
            denoising = 'robust'
        module = Module04(file_name_1, file_name_2, type_filter, gamma, self.mask, self.filter_size)
        rmse = module.main()
        print('Img 2 (21.875998861123463): %.4f' % round(rmse, 4))

    def img3(self):
        ## img3
        file_name_1 = 'input_image/moon_small.jpg'
        file_name_2 = 'input_image/case3_70.png'
        type_filter = 1
        gamma = 0.8

        if type_filter == 1:  # method 1
            self.mask = '0 20 0 30'
            self.filter_size = 3
            self.denoising = 'robust'
        module = Module04(file_name_1, file_name_2, type_filter, gamma, self.mask, self.filter_size)
        rmse = module.main()
        print('Img 3 (22.21161211822827): %.4f' % round(rmse, 4))

    def img4(self):
        ## img4
        file_name_1 = 'input_image/moon_small.jpg'
        file_name_2 = 'input_image/case3_70.png'
        type_filter = 1
        gamma = 1.0

        if type_filter == 1:  # method 1
            self.mask = '0 20 0 30'
            self.filter_size = 3
            self.denoising = 'average'
        module = Module04(file_name_1, file_name_2, type_filter, gamma, self.mask, self.filter_size)
        rmse = module.main()
        print('Img 4 (21.677498481828746): %.4f' % round(rmse, 4))

    def img5(self):
        # img6
        file_name_1 = 'input_image/polygons128.png'
        file_name_2 = 'case5_5_1.png'
        type_filter = 2
        gamma = 0.00005

        if type_filter == 2:  # method 2
            self.filter_size = 5
            self.sigma = 1.0
        module = Module04(file_name_1, file_name_2, type_filter, gamma, self.mask, self.filter_size, self.denoising,
                          self.sigma)
        rmse = module.main()
        print('Img 5 (6.477146761945513): %.4f' % round(rmse, 4))

    def img6(self):
        # img6
        file_name_1 = 'input_image/polygons128.png'
        file_name_2 = 'input_image/case6_3_1.png'
        type_filter = 2
        gamma = 0.02

        if type_filter == 2:  # method 2
            self.filter_size = 3
            self.sigma = 1.0
        module = Module04(file_name_1, file_name_2, type_filter, gamma, self.mask, self.filter_size, self.denoising,
                          self.sigma)
        rmse = module.main()
        print('Img 6 (36.97933547863464): %.4f' % round(rmse, 4))

    def img7(self):
        # img6
        file_name_1 = 'input_image/moon.jpg'
        file_name_2 = 'case7_7_125.png'
        type_filter = 2
        gamma = 0.00008

        if type_filter == 2:  # method 2
            self.filter_size = 7
            self.sigma = 1.25
        module = Module04(file_name_1, file_name_2, type_filter, gamma, self.mask, self.filter_size, self.denoising,
                          self.sigma)
        rmse = module.main()
        print('Img 7 (3.6887928910639673): %.4f' % round(rmse, 4))

    def img8(self):
        # img6
        file_name_1 = 'input_image/moon.jpg'
        file_name_2 = 'case8_3_15.png'
        type_filter = 2
        gamma = 0.008

        if type_filter == 2:  # method 2
            self.filter_size = 3
            self.sigma = 1.6
        module = Module04(file_name_1, file_name_2, type_filter, gamma, self.mask, self.filter_size, self.denoising,
                          self.sigma)
        rmse = module.main()
        print('Img 8 (13.31402607798533): %.4f' % round(rmse, 4))

    def main(self):
        ### -- deploy - debug -- ###
        #### Start
        self.img1()
        self.img2()
        self.img3()
        self.img4()
        self.img5()
        self.img6()
        self.img7()
        self.img8()
        ## End


if __name__ == '__main__':
    mask = str()
    filter_size = int()
    denoising = str()
    sigma = float()

    file_name_1 = str(input()).rstrip()  # File name input - reference image
    file_name_2 = str(input()).rstrip()  # File name input - degraded image
    type_filter = int(input())  # Type of filter method
    gamma = float(input())  # Parameter gamma - γ ∈ [0, 1]

    if type_filter == 1:  # method 1
        mask = str(input().strip())  # Mask whit size 0-4
        filter_size = int(input().strip())  # n ∈ [3, 5, 7, 9, 11]
        denoising = str(input().strip())  # denoising mode: “average” or “robust”
    elif type_filter == 2:  # method 2
        filter_size = int(input())  # gaussian filter k ∈ [3, 5, 7, 9, 11]
        sigma = float(input())  # σ for the gaussian filter

    module = Module04(file_name_1, file_name_2, type_filter, gamma, mask, filter_size, denoising, sigma)
    rmse = module.main()
    print('%.4f' % round(rmse, 4))

    ### -- deploy - debug -- ###
    #### Start

    # dbug = Debug()
    # dbug.main()

    ## End
